var arregloPaises=["Peru","Germany"];
var tipoDato;
var arregloURI=new Array();
var rows=new Array();
var arregloDataPaises=new Array();
//var rowColumn=new Array();
//var rowData=new Array();


var longestLengthCountry="Ireland";
var uri;	      
//var uriBase="https://api.covid19api.com/country/";
var uriBase="https://api.covid19api.com/total/dayone/country/";
var lengthJsonAnterior=0;
var longestLengthIndice=0;
var longestLength=0;

var ordenDiasHorizontal;

var arregloTiposDato=new Array();

google.charts.load('current', {'packages':['corechart']});
google.charts.load('current', {'packages':['line']});


document.getElementById("btnCountries").addEventListener("click", function(){

	if(agregarPais()){
		drawChart();
	}
});
	      

document.getElementById("btnRemoveCountries").addEventListener("click", function(){

	if(removerPais()){
		
		drawChart();
	}
});


document.getElementById("btnActualizar").addEventListener("click", function(){

	drawChart();
});


//Setup radioButtons eje horizontal
document.getElementById("ordenadoFecha").checked = true;
var radioButtonHorizontal=document.forms['formSeleccionarDiasOrden'].elements['ordenDias'];
ordenDiasHorizontal=radioButtonHorizontal[0].value;

radioButtonHorizontal[0].onclick=function(){
	ordenDiasHorizontal=radioButtonHorizontal[0].value;
	drawChart();
};
radioButtonHorizontal[1].onclick=function(){
	ordenDiasHorizontal=radioButtonHorizontal[1].value;
	drawChart();
};



//Setup radioButtons eje vertical
document.getElementById("Confirmados").checked = true;
var radioButtonVertical=document.forms['formSeleccionarDatoVertical'].elements['sDatoVertical'];
tipoDato=radioButtonVertical[0].value;
arregloTiposDato=[radioButtonVertical[0],radioButtonVertical[1],radioButtonVertical[2],radioButtonVertical[3]];
radioButtonVertical[0].onclick=function(){
	tipoDato=radioButtonVertical[0].value;
	drawChart();
};
radioButtonVertical[1].onclick=function(){
	tipoDato=radioButtonVertical[1].value;
	drawChart();
};
radioButtonVertical[2].onclick=function(){
	tipoDato=radioButtonVertical[2].value;
	drawChart();
};
radioButtonVertical[3].onclick=function(){
	tipoDato=radioButtonVertical[3].value;
	drawChart();
};



cargarListaPaisesForm();
  
google.charts.setOnLoadCallback();

pintarSeleccionadosPaisesForm();

drawChart();



function cargarListaPaisesForm(){
	$.ajax({url:"https://api.covid19api.com/summary",async: false}).then(function(data_json){
		var i;
		
		countries=data_json.Countries;
		//console.log(data_json);
		for(i=0;i<countries.length;i++){
			//console.log(countries[i].Country);
			var opt = document.createElement('option');
			paisNoTraducido=countries[i].Country;
			paisTraducido=translateCountryManualmente(paisNoTraducido);
			paisTraducido=paisTraducido+"  - ["+data_json.Countries[i].CountryCode+"]";
			opt.appendChild( document.createTextNode(paisTraducido));
			opt.value = paisNoTraducido;
			opt.id=paisNoTraducido;
			opt.name=paisNoTraducido;
			opt.style.backgroundColor='#FFFFFF';
			document.getElementById('anotherCuntries').appendChild(opt);

		}
	});
	
}
function pintarSeleccionadosPaisesForm(){
	var f;

	for(f=0;f<arregloPaises.length;f++){

		pintarPaisesForm(arregloPaises[f]);
	}
	
}

function pintarPaisesForm(pais){
	document.getElementById(String(pais)).style.background='#009B7D';
}
function despintarPaisesForm(pais){
	document.getElementById(pais).style.background='#FFFFFF';
}


function translateCountryManualmente(countryname){
	return countryname;
}
function translateCountryAPI(countryName){
	
	var settings = {
			"async": true,
			"crossDomain": true,
			"url": "https://google-translate1.p.rapidapi.com/language/translate/v2",
			"method": "POST",
			"headers": {
				"x-rapidapi-host": "google-translate1.p.rapidapi.com",
				"x-rapidapi-key": "38b284be66mshb57cfefd43e8135p1cde24jsnf48316049340",
				"accept-encoding": "application/gzip",
				"content-type": "application/x-www-form-urlencoded"
			},
			"data": {
				"source": "en",
				"q": countryName,
				"target": "es"
			}
		}
	
		$.ajax(settings).done(function (response) {
			return (response.data.translations[0].translatedText);
		});	
}
function agregarPais() {
	operacion=0;
	pais=document.getElementById("anotherCuntries").value;
	if ((arregloPaises.indexOf(pais) != -1)||(pais=="")){
		alert("País seleccionado no es válido");
		operacion=0;
	} else {
		arregloPaises.push(pais);		
		console.log("Pais2:");
		console.log(pais);
		pintarPaisesForm(pais);
		operacion=1;
	};
	return operacion;
}

function removerPais() {
	operacion=0;
	pais=document.getElementById("anotherCuntries").value;
	if (arregloPaises.indexOf(pais) != -1){
		variableCountry=arregloPaises[arregloPaises.indexOf(pais)];
		despintarPaisesForm(variableCountry);
		var varArregloPaises= new Array();
		for (k = 0; k < (arregloPaises.length) ; k++){			
			if(arregloPaises[k]!=variableCountry){
				varArregloPaises.push(arregloPaises[k])
			}			
		}
		arregloPaises=varArregloPaises;
		operacion=1;

	}else{
		alert("País seleccionado no es valido1");
		operacion=0;
	};
	
	if (arregloPaises.length==0){
		if(pais=="Peru"){
			operacion=0;
			alert("Perú es el pais por defecto, no se permite removerlo.");
		} else{
			alert("No hay paises seleccionados, se muestra a Perú por defecto.");
			operacion=1;
		}	
		arregloPaises=["Peru"];
		pintarPaisesForm("Peru");
	}
	
	return operacion;
	
}

function removerPaisNombre(pais) {
	operacion=0;
	if (arregloPaises.indexOf(pais) != -1){
		variableCountry=arregloPaises[arregloPaises.indexOf(pais)];
		despintarPaisesForm(pais);
		var varArregloPaises= new Array();
		for (k = 0; k < (arregloPaises.length) ; k++){			
			if(arregloPaises[k]!=variableCountry){
				varArregloPaises.push(arregloPaises[k])
			}			
		}
		arregloPaises=varArregloPaises;
		operacion=1;
	}else{
		alert("País seleccionado no valido2");
		operacion=0;
	};
	return operacion;
}
	

function drawChart() {
	rows=new Array();
	//rowColumn=new Array();
	//rowData=new Array();
	longestLengthIndice=0;
	longestLengthCountry=0;
	longestLength=0;
	lengthJsonAnterior=0;
	
	
	updateUriArray();
	
	determinar_longest_array_country();
	
	setup_chart_array();	
			
	JsonAllCountriesRecordIt();
	
	graficarConRaw();
	
}
function updateUriArray(){
	console.log("Iniciando updateUriArray");
	longestLength=0;
	for(h=0;h<arregloPaises.length;h++){
			uri= uriBase.concat(arregloPaises[h]);
			arregloURI[h]=uri;
			
	}
	//console.log("arregloURI");
	//console.log(arregloURI);
	console.log("Finalizando updateUriArray");
}

function determinar_longest_array_country(){
	console.log("Iniciando determinar_longest_array_country");
	var defer = $.Deferred();
	arregloPaises.forEach(function(elemento,indice,array){
		$.ajax({url:arregloURI[indice],async: false}).then(function(data_json){
			//console.log("A_Valor propuesto:");
			//console.log(data_json.length);
				if(lengthJsonAnterior<=data_json.length){
					longestLengthIndice=indice;
					longestLengthCountry=elemento;
					longestLength=data_json.length;
					lengthJsonAnterior=data_json.length;
					//console.log("A_Nuevo valor alto:");
					//console.log(lengthJsonAnterior);
				    //console.log(indice);
				    //console.log(arregloPaises.length-1);
				    
				}					
						    	    	  		  
		//});
		});
	});
	console.log("Iniciando determinar_longest_array_country");
	defer.resolve(); 
	return defer;
}

function setup_chart_array(){
	console.log("Inicializando setup_chart_array");	
	var defer = $.Deferred();
	$.ajax({url:uri,async: false}).then(function(data_json){
		//console.log("B_Inicialización de datos de array con el length");
		//console.log(longestLength);
		

		rows2 = new Array(longestLength+1);         
		for(var n = 0; n < rows2.length; n++)
			rows2[n] = new Array(arregloPaises.length+1);           
		rows2[0][0]="Date";
		rows=rows2.slice();
		
		/*//rowData y rowColumn
		rows3 = new Array(longestLength);              
		for(var n = 0; n < rows3.length; n++)
			rows3[n] = new Array(arregloPaises.length+1);
		rowData=rows3.slice();
		
	
		rows4 = new Array(arregloPaises.length+1);    
		rows4[0]="Date";
		rowColumn=rows4.slice();
		*/
		//console.log("B_longestLength");
	    //console.log(longestLength);
	});
	console.log("Finalizando setup_chart_array");
	defer.resolve(); 
	return defer;
}
function JsonAllCountriesRecordIt(){
	var defer = $.Deferred();
	arregloPaises.forEach(function(elemento,indice,array){
		$.ajax({url:arregloURI[indice],async: false}).then(function(data_json){
			console.log(elemento);
			console.log("C_JSON_grabar_datos")
			console.log(data_json);
			//console.log("C_----------------------------------------------------------------------");
			if (data_json.length==0){
				removerPaisNombre(pais);
				despintarPaisesForm(pais);
				mensaje="Lo sentimos, actualmente no contamos con información de "+pais+".";
				alert(mensaje);
			} else{
				grabarCountryJsonChartArray(data_json,indice);
			}		
		});
	});
	defer.resolve(); 
	return defer;
}

function grabarCountryJsonChartArray(data_json,indice){
	var cant_data=(data_json.length);
	/*
	for (i = 1; i < (cant_data+1) ; i++){
		
		if(indice==longestLengthIndice){
		
			date = new Date(data_json[cant_data-i].Date);
			date.setDate(date.getDate()+1);
			year = date.getFullYear();
			month = date.getMonth()+1;
			dt = date.getDate();
	
			if (dt < 10) {
			  dt = '0' + dt;
			}
			if (month < 10) {
			  month = '0' + month;
			}
	
			var dateFormat=(dt+'-' + month + '-'+year);
	
			if(ordenDiasHorizontal=="ordenadoFecha"){
				rows[cant_data+1-i][0]=dateFormat;
				//rowData[cant_data-i][0]=dateFormat;
			}
			if(ordenDiasHorizontal=="desdeCasoCero"){
				numeroDia=cant_data+1-i;
				rows[cant_data+1-i][0]=numeroDia;
				//rowData[cant_data-i][0]=dateFormat;
			}
		}
		
		
		if(ordenDiasHorizontal=="desdeCasoCero"){
			//GRAFICO PARA CURVAS QUE EMPIEZAN DESDE CERO
			if(tipoDato=="Confirmados"){
				rows[cant_data+1-i][indice+1]=data_json[cant_data-i].Confirmed;
				//rowData[cant_data-i][indice+1]=data_json[cant_data-i].Confirmed;
				if((i==1) && (data_json[cant_data-i].Confirmed==0) ){rows[cant_data+1-i][indice+1]=data_json[cant_data-i-1].Confirmed;}
			}
			if(tipoDato=="Muertes"){
				rows[cant_data+1-i][indice+1]=data_json[cant_data-i].Deaths;
				//rowData[cant_data-i][indice+1]=data_json[cant_data-i].Deaths;
				if((i==1) && (data_json[cant_data-i].Deaths==0) ){rows[cant_data+1-i][indice+1]=data_json[cant_data-i-1].Deaths;}
			}
			if(tipoDato=="Recuperados"){
				rows[cant_data+1-i][indice+1]=data_json[cant_data-i].Recovered;
				//rowData[cant_data-i][indice+1]=data_json[cant_data-i].Recovered;
				if((i==1) && (data_json[cant_data-i].Recovered==0) ){rows[cant_data+1-i][indice+1]=data_json[cant_data-i-1].Recovered;}
			}
			if(tipoDato=="Activos"){
				rows[cant_data+1-i][indice+1]=data_json[cant_data-i].Active;
				//rowData[cant_data-i][indice+1]=data_json[cant_data-i].Active;
				if((i==1) && (data_json[cant_data-i].Active==0) ){rows[cant_data+1-i][indice+1]=data_json[cant_data-i-1].Active;}
			}
		}
		
		if(ordenDiasHorizontal=="ordenadoFecha"){
			//GRAFICO PARA CURVAS CON FECHA CORRECTA
			diferencia=longestLength-cant_data;
			if(tipoDato=="Confirmados"){
				rows[diferencia+cant_data+1-i][indice+1]=data_json[cant_data-i].Confirmed;
				//rowData[diferencia+cant_data-i][indice+1]=data_json[cant_data-i].Confirmed;
				if((i==1) && (data_json[cant_data-i].Confirmed==0) ){rows[diferencia+cant_data+1-i][indice+1]=data_json[cant_data-i-1].Confirmed;}
			}
			if(tipoDato=="Muertes"){
				rows[diferencia+cant_data+1-i][indice+1]=data_json[cant_data-i].Deaths;
				//rowData[diferencia+cant_data-i][indice+1]=data_json[cant_data-i].Deaths;
				if((i==1) && (data_json[cant_data-i].Deaths==0) ){rows[diferencia+cant_data+1-i][indice+1]=data_json[cant_data-i-1].Deaths;}
			}
			if(tipoDato=="Recuperados"){
				rows[diferencia+cant_data+1-i][indice+1]=data_json[cant_data-i].Recovered;
				//rowData[diferencia+cant_data-i][indice+1]=data_json[cant_data-i].Recovered;
				if((i==1) && (data_json[cant_data-i].Recovered==0) ){rows[diferencia+cant_data+1-i][indice+1]=data_json[cant_data-i-1].Recovered;}
			}
			if(tipoDato=="Activos"){
				rows[diferencia+cant_data+1-i][indice+1]=data_json[cant_data-i].Active;
				//rowData[diferencia+cant_data-i][indice+1]=data_json[cant_data-i].Active;
				if((i==1) && (data_json[cant_data-i].Active==0) ){rows[diferencia+cant_data+1-i][indice+1]=data_json[cant_data-i-1].Active;}
			}
			
		}
		
		if((i==cant_data) && (indice==(arregloPaises.length-1))){

			arregloPaises.forEach(function(elemento,indice,array){
					paisNoTraducido=elemento;
					paisTraducido2=translateCountryManualmente(paisNoTraducido);
					paisTraducido=paisTraducido2;
    				rows[0][indice+1]=paisTraducido;
    				//rowColumn[indice+1]=paisTraducido;    			   	    	     				
			});	
		}			
	}
	*/	
	
	
	for (i = longestLength-cant_data; i < (longestLength) ; i++){		
		
		var ordenDesdeCero=i+cant_data-longestLength;
		
		if(indice==longestLengthIndice){
			
			date = new Date(data_json[ordenDesdeCero].Date);
			date.setDate(date.getDate()+1);
			year = date.getFullYear();
			month = date.getMonth()+1;
			dt = date.getDate();
	
			if (dt < 10) {
			  dt = '0' + dt;
			}
			if (month < 10) {
			  month = '0' + month;
			}
	
			var dateFormat=(dt+'-' + month + '-'+year);
	
			if(ordenDiasHorizontal=="ordenadoFecha"){
				rows[ordenDesdeCero+1][0]=dateFormat;
				//rowData[ordenDesdeCero][0]=dateFormat;
			}
			if(ordenDiasHorizontal=="desdeCasoCero"){
				numeroDia=ordenDesdeCero+1;
				rows[ordenDesdeCero+1][0]=numeroDia;
				//rowData[ordenDesdeCero][0]=numeroDia;
			}
		}		
		
		if(ordenDiasHorizontal=="ordenadoFecha"){
			//GRAFICO PARA CURVAS CON FECHA CORRECTA
			if(tipoDato=="Confirmados"){
				rows[i+1][indice+1]=data_json[ordenDesdeCero].Confirmed;
				//rowData[i][indice+1]=data_json[ordenDesdeCero].Confirmed;
				if((data_json[ordenDesdeCero].Confirmed==0) && (ordenDesdeCero!=0)){rows[i+1][indice+1]=data_json[ordenDesdeCero-1].Confirmed;}
			}
			if(tipoDato=="Muertes"){
				rows[i+1][indice+1]=data_json[ordenDesdeCero].Deaths;
				//rowData[i][indice+1]=data_json[ordenDesdeCero].Deaths;
				if((data_json[ordenDesdeCero].Deaths==0) && (ordenDesdeCero!=0)){rows[i+1][indice+1]=data_json[ordenDesdeCero-1].Deaths;}
			}
			if(tipoDato=="Recuperados"){
				rows[i+1][indice+1]=data_json[ordenDesdeCero].Recovered;
				//rowData[i][indice+1]=data_json[ordenDesdeCero].Recovered;
				if((data_json[ordenDesdeCero].Recovered==0) && (ordenDesdeCero!=0)){rows[i+1][indice+1]=data_json[ordenDesdeCero-1].Recovered;}
			}
			if(tipoDato=="Activos"){
				
				rows[i+1][indice+1]=data_json[ordenDesdeCero].Active;
				//rowData[i][indice+1]=data_json[ordenDesdeCero].Active;
				
				if(ordenDesdeCero!=0){
					if(((data_json[ordenDesdeCero].Deaths==0) && (data_json[ordenDesdeCero-1].Deaths!=0))||((data_json[ordenDesdeCero].Recovered==0) && (data_json[ordenDesdeCero-1].Recovered!=0))) 
					rows[i+1][indice+1]=data_json[ordenDesdeCero-1].Active;
				}
			}
		}
		
		if(ordenDiasHorizontal=="desdeCasoCero"){
			
			//GRAFICO PARA CURVAS QUE EMPIEZAN DESDE CERO
			diferencia=longestLength-cant_data;
			if(tipoDato=="Confirmados"){
				rows[ordenDesdeCero+1][indice+1]=data_json[ordenDesdeCero].Confirmed;
				//rowData[ordenDesdeCero][indice+1]=data_json[ordenDesdeCero].Confirmed;
				if((data_json[ordenDesdeCero].Confirmed==0) && (ordenDesdeCero!=0)){rows[ordenDesdeCero+1][indice+1]=data_json[ordenDesdeCero-1].Confirmed;}
			}
			if(tipoDato=="Muertes"){
				rows[ordenDesdeCero+1][indice+1]=data_json[ordenDesdeCero].Deaths;
				//rowData[ordenDesdeCero][indice+1]=data_json[ordenDesdeCero].Deaths;
				if((data_json[ordenDesdeCero].Deaths==0) && (ordenDesdeCero!=0)){rows[ordenDesdeCero+1][indice+1]=data_json[ordenDesdeCero-1].Deaths;}
			}
			if(tipoDato=="Recuperados"){
				rows[ordenDesdeCero+1][indice+1]=data_json[ordenDesdeCero].Recovered;
				//rowData[ordenDesdeCero][indice+1]=data_json[ordenDesdeCero].Recovered;
				if((data_json[ordenDesdeCero].Recovered==0) && (ordenDesdeCero!=0)){rows[ordenDesdeCero+1][indice+1]=data_json[ordenDesdeCero-1].Recovered;}
			}
			if(tipoDato=="Activos"){
				rows[ordenDesdeCero+1][indice+1]=data_json[ordenDesdeCero].Active;
				//rowData[ordenDesdeCero][indice+1]=data_json[ordenDesdeCero].Active;
				if(ordenDesdeCero!=0){
					if(( (data_json[ordenDesdeCero].Deaths==0) && (data_json[ordenDesdeCero-1].Deaths!=0))||((data_json[ordenDesdeCero].Recovered==0) && (data_json[ordenDesdeCero-1].Recovered!=0))) 
						rows[ordenDesdeCero+1][indice+1]=data_json[ordenDesdeCero-1].Active;
				}
			}			
		}
		if((i==longestLength-1) && (indice==(arregloPaises.length-1))){

			arregloPaises.forEach(function(elemento,indice,array){
					paisNoTraducido=elemento;
					paisTraducido2=translateCountryManualmente(paisNoTraducido);
					paisTraducido=paisTraducido2;
    				rows[0][indice+1]=paisTraducido;
    				//rowColumn[indice+1]=paisTraducido;    			   	    	     				
			});	
		}	
	}
	
}

function graficarConRaw(){
	var defer = $.Deferred();
	$.ajax({url:uri}).then(function(data_json){
	//var data_chart2 = new google.visualization.DataTable();
	//for (b = 0; b < (rowColumn.length) ; b++){
	//	if(b==0){
	//		data_chart2.addColumn('string', 'Date');
	//	}
	//	else{
	//		data_chart2.addColumn('number', rowColumn[b]);
	//	}
	//}
	console.log("D_rows");
	console.log(rows);
	//data_chart2.addRows(rowData);
	var data_chart3 = google.visualization.arrayToDataTable(rows);
	
	var titulo_2=  tipoDato +' vs. '+ ordenDiasHorizontal;
      var options_2 = {
          //title: titulo_2,
          hAxis: {title: 'Días'},
          vAxis: {title: 'Cantidad'},
          legend: { position: 'right' }
      };
	//var chart3 = new google.charts.Line(document.getElementById('chart_div'));
		        	
	var chart3 = new google.visualization.LineChart(document.getElementById('chart_div'));

	chart3.draw(data_chart3,  google.charts.Line.convertOptions(options_2));
	});
	defer.resolve(); 
	return defer;
}


function drawChartOneCountry(countryP,tipoDatoVerticalP) {
  	var tipoDatoVerticalPais=tipoDatoVerticalP;
		var country=countryP;
		uriBase="https://api.covid19api.com/country/";
		uri= uriBase.concat(country);
		var rows = new Array();
		
		
  $(document).ready(function() {
	$.ajax({url:uri}).then(function(data_json){
		
		var cant_data=(data_json.length);
		
		var data_chart = new google.visualization.DataTable();
		data_chart.addColumn('string', 'Date');
		data_chart.addColumn('number', tipoDatoVerticalPais);
		
						
		for (i = 0; i < cant_data ; i++) {
			date = new Date(data_json[i].Date);
			year = date.getFullYear();
			month = date.getMonth()+1;
			dt = date.getDate();

			if (dt < 10) {
			  dt = '0' + dt;
			}
			if (month < 10) {
			  month = '0' + month;
			}

			var a1=(dt+'-' + month + '-'+year);
			
			var a2=data_json[i].Confirmed;
			rows.push([a1,a2]);				
		}
		data_chart.addRows(rows);
		
		  var titulo= 'Casos confirmados vs. tiempo - '+tipoDatoVerticalPais+ ' - '+country
          var options = {
            title: titulo,
            hAxis: {title: 'Fechas'},
            vAxis: {title: 'Casos confirmados'},
            legend: 'none'
          };

          var chart2 = new google.visualization.ScatterChart(document.getElementById('chart_div'));

          chart2.draw(data_chart, options);          
	
  });
  
  });
}