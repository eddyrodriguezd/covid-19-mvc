INSERT INTO sintomas (nombre) VALUES ('Tos');
INSERT INTO sintomas (nombre) VALUES ('Disminución del olfato');
INSERT INTO sintomas (nombre) VALUES ('Fiebre');
INSERT INTO sintomas (nombre) VALUES ('Dolor de garganta');
INSERT INTO sintomas (nombre) VALUES ('Dificultad para respirar');
INSERT INTO sintomas (nombre) VALUES ('Congestión nasal');

INSERT INTO condiciones (nombre) VALUES ('Obesidad');
INSERT INTO condiciones (nombre) VALUES ('Enfermedades pulmonares');
INSERT INTO condiciones (nombre) VALUES ('Asma');
INSERT INTO condiciones (nombre) VALUES ('Diabetes');
INSERT INTO condiciones (nombre) VALUES ('Hipertensión');
INSERT INTO condiciones (nombre) VALUES ('Enfermedades inmunosupresoras');
INSERT INTO condiciones (nombre) VALUES ('Enfermedades cardiovasculares');
INSERT INTO condiciones (nombre) VALUES ('Insuficiencia renal');
INSERT INTO condiciones (nombre) VALUES ('Cáncer');

INSERT INTO usuarios (nombre, apellido, num_id_nacional, fecha_nacimiento, sexo, pais, ciudad, direccion, num_celular, correo) VALUES ('Eddy', 'Rodríguez', '76444245', '1997-10-18', 'Masculino', 'Perú', 'Lima', 'Av. José Granda 3194', 934567603, 'e.rodriguezd@pucp.edu.pe');
INSERT INTO usuarios (nombre, apellido, num_id_nacional, fecha_nacimiento, sexo, pais, ciudad, direccion, num_celular, correo) VALUES ('Jhon', 'Velasquez', '74777843', '1998-07-19', 'Masculino', 'Perú', 'Lima', 'Av. Varela', 997192426, 'jhon.velasquez@pucp.pe');

INSERT INTO tests_virtuales (fecha, trabaja_salud, contacto_contagiado, usuario_id, resultado) VALUES ('2020-02-15', 0, 0, 1, 'Riesgo Bajo');
INSERT INTO tests_virtuales (fecha, trabaja_salud, contacto_contagiado, usuario_id, resultado) VALUES ('2020-04-02', 0, 0, 1, 'Riesgo Bajo');
INSERT INTO tests_virtuales (fecha, trabaja_salud, contacto_contagiado, usuario_id, resultado) VALUES ('2020-06-13', 0, 1, 1, 'Riesgo Medio');

INSERT INTO tests_virtuales_sintomas (test_id, sintoma_id) VALUES (1, 1);
INSERT INTO tests_virtuales_sintomas (test_id, sintoma_id) VALUES (2, 1);
INSERT INTO tests_virtuales_sintomas (test_id, sintoma_id) VALUES (2, 4);

INSERT INTO tests_virtuales_sintomas (test_id, sintoma_id) VALUES (3, 1);
INSERT INTO tests_virtuales_sintomas (test_id, sintoma_id) VALUES (3, 3);
INSERT INTO tests_virtuales_sintomas (test_id, sintoma_id) VALUES (3, 6);

INSERT INTO tests_virtuales_condiciones (test_id, condicion_id) VALUES (1, 3);
INSERT INTO tests_virtuales_condiciones (test_id, condicion_id) VALUES (2, 3);
INSERT INTO tests_virtuales_condiciones (test_id, condicion_id) VALUES (3, 3);

INSERT INTO tests_clinicos (fecha, tipo, resultado, usuario_id) VALUES ('2020-06-18', 'Prueba rápida', 'No reactivo', 1);

INSERT INTO users (mail, password, enabled) VALUES ('e.rodriguezd@pucp.edu.pe', '$2y$12$/NZ1wKsw5TG5G4ggW3NTvur.bzt2UdBApCC1pC51cn/cUO/jSC52m', 1);
INSERT INTO authorities (user_id, authority) VALUES (1, 'ROLE_USER');
INSERT INTO users (mail, password, enabled) VALUES ('jhon.velasquez@pucp.pe', '$2y$12$ClwFJHKzWtaOc4UJlgLWbuzW6FyQZxAsWal5fyO90OHd3Yd0/xMuK', 1);
INSERT INTO authorities (user_id, authority) VALUES (2, 'ROLE_USER');