package com.covid19mvc.app.model.entities.publico;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DatosAPI{
	
	@JsonProperty("Global")
	DatosGlobales global;
	
	@JsonProperty("Countries")
    PaisAPI[] countries;
    
    public DatosAPI() {
    }
	
	public DatosGlobales getGlobal() {
		return global;
	}

	public void setGlobal(DatosGlobales global) {
		this.global = global;
	}
	

	public PaisAPI[] getCountries() {
		return countries;
	}

	public void setCountries(PaisAPI[] countries) {
		this.countries = countries;
	}

	public static class DatosGlobales{
		
		@JsonProperty("NewConfirmed")
		private Long newConfirmed;
		
		@JsonProperty("TotalConfirmed")
		private Long totalConfirmed;
		
		@JsonProperty("NewDeaths")
		private Long newDeaths;
		
		@JsonProperty("TotalDeaths")
		private Long totalDeaths;
		
		@JsonProperty("NewRecovered")
		private Long newRecovered;
		
		@JsonProperty("TotalRecovered")
		private Long totalRecovered;
		
		public DatosGlobales() {
		}

		public Long getNewConfirmed() {
			return newConfirmed;
		}

		public void setNewConfirmed(Long newConfirmed) {
			this.newConfirmed = newConfirmed;
		}

		public Long getTotalConfirmed() {
			return totalConfirmed;
		}

		public void setTotalConfirmed(Long totalConfirmed) {
			this.totalConfirmed = totalConfirmed;
		}

		public Long getNewDeaths() {
			return newDeaths;
		}

		public void setNewDeaths(Long newDeaths) {
			this.newDeaths = newDeaths;
		}

		public Long getTotalDeaths() {
			return totalDeaths;
		}

		public void setTotalDeaths(Long totalDeaths) {
			this.totalDeaths = totalDeaths;
		}

		public Long getNewRecovered() {
			return newRecovered;
		}

		public void setNewRecovered(Long newRecovered) {
			this.newRecovered = newRecovered;
		}

		public Long getTotalRecovered() {
			return totalRecovered;
		}

		public void setTotalRecovered(Long totalRecovered) {
			this.totalRecovered = totalRecovered;
		}
	
	}

	public static class PaisAPI{
		
		@JsonProperty("Country")
    	private String country; //ALA Aland Islands
		
		@JsonProperty("CountryCode")
        private String countryCode; //AX
		
		@JsonProperty("Slug")
        private String slug; //ala-aland-islands
		
		@JsonProperty("NewConfirmed")
        private Long newConfirmed; //0
		
		@JsonProperty("TotalConfirmed")
        private Long totalConfirmed; //0
		
		@JsonProperty("NewDeaths")
        private Long newDeaths; //0
		
		@JsonProperty("TotalDeaths")
        private Long totalDeaths; //0
		
		@JsonProperty("NewRecovered")
        private Long newRecovered; //0
		
		@JsonProperty("TotalRecovered")
        private Long totalRecovered; //0
		
		@JsonProperty("Date")
        private Date date; //2020-04-05T06:37:00Z
        
		public PaisAPI() {
		}

		public String getCountry() {
			return country;
		}

		public void setCountry(String country) {
			this.country = country;
		}

		public String getCountryCode() {
			return countryCode;
		}

		public void setCountryCode(String countryCode) {
			this.countryCode = countryCode;
		}

		public String getSlug() {
			return slug;
		}

		public void setSlug(String slug) {
			this.slug = slug;
		}

		public Long getNewConfirmed() {
			return newConfirmed;
		}

		public void setNewConfirmed(Long newConfirmed) {
			this.newConfirmed = newConfirmed;
		}

		public Long getTotalConfirmed() {
			return totalConfirmed;
		}

		public void setTotalConfirmed(Long totalConfirmed) {
			this.totalConfirmed = totalConfirmed;
		}

		public Long getNewDeaths() {
			return newDeaths;
		}

		public void setNewDeaths(Long newDeaths) {
			this.newDeaths = newDeaths;
		}

		public Long getTotalDeaths() {
			return totalDeaths;
		}

		public void setTotalDeaths(Long totalDeaths) {
			this.totalDeaths = totalDeaths;
		}

		public Long getNewRecovered() {
			return newRecovered;
		}

		public void setNewRecovered(Long newRecovered) {
			this.newRecovered = newRecovered;
		}

		public Long getTotalRecovered() {
			return totalRecovered;
		}

		public void setTotalRecovered(Long totalRecovered) {
			this.totalRecovered = totalRecovered;
		}

		public Date getDate() {
			return date;
		}

		public void setDate(Date date) {
			this.date = date;
		}
      
    }
    
}
