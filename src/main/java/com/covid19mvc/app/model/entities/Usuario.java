package com.covid19mvc.app.model.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "usuarios")
public class Usuario implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotEmpty
	private String nombre;
	
	@NotEmpty
	private String apellido;
	
	@NotEmpty
	@Column(name = "num_id_nacional")
	private String numIdNacional;
	
	@NotNull
	@Column(name = "fecha_nacimiento")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date fechaNacimiento;
	
	@NotEmpty
	private String sexo;
	
	@NotEmpty
	private String pais;

	@NotEmpty	
	private String ciudad;

	@NotEmpty
	private String direccion;
	
	@Column(name = "num_celular")
	private Long numCelular;
	
	@NotEmpty
	private String correo;
	
	@OneToMany(fetch= FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinColumn(name = "usuario_id", nullable = false)
	private List<TestDescarteClinico> testsClinicos;
	
	@OneToMany(fetch= FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinColumn(name = "usuario_id", nullable = false)
	private List<TestDescarteVirtual> testsVirtuales;
	
	public Usuario() {
		testsClinicos = new ArrayList<>();
		testsVirtuales = new ArrayList<>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNumIdNacional() {
		return numIdNacional;
	}

	public void setNumIdNacional(String numIdNacional) {
		this.numIdNacional = numIdNacional;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Long getNumCelular() {
		return numCelular;
	}

	public void setNumCelular(Long numCelular) {
		this.numCelular = numCelular;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public List<TestDescarteClinico> getTestsClinicos() {
		return testsClinicos;
	}

	public void setTestsClinicos(List<TestDescarteClinico> testsClinicos) {
		this.testsClinicos = testsClinicos;
	}

	public List<TestDescarteVirtual> getTestsVirtuales() {
		return testsVirtuales;
	}

	public void setTestsVirtuales(List<TestDescarteVirtual> testsVirtuales) {
		this.testsVirtuales = testsVirtuales;
	}
	
	public void addTestClinico(TestDescarteClinico testClinico) {
		testsClinicos.add(testClinico);
	}
	
	public void addTestVirtual(TestDescarteVirtual testVirtual) {
		testsVirtuales.add(testVirtual);
	}
}
