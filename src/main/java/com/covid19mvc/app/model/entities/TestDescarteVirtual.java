package com.covid19mvc.app.model.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "tests_virtuales")
public class TestDescarteVirtual implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date fecha;
	
	@NotNull
	@Column(name = "trabaja_salud")
	private Boolean trabajaSalud;
	
	@NotNull
	@Column(name = "contacto_contagiado")
	private Boolean contactoContagiado;
	
	@ManyToMany
	@JoinTable(
	  name = "tests_virtuales_sintomas", 
	  joinColumns = @JoinColumn(name = "test_id"), 
	  inverseJoinColumns = @JoinColumn(name = "sintoma_id"))
	private List<Sintoma> sintomas;
	
	@ManyToMany
	@JoinTable(
	  name = "tests_virtuales_condiciones", 
	  joinColumns = @JoinColumn(name = "test_id"), 
	  inverseJoinColumns = @JoinColumn(name = "condicion_id"))
	private List<Condicion> condiciones;
	
	private String resultado;
	
	@PrePersist
	public void prePersist() {
		fecha = new Date();
	}

	public TestDescarteVirtual() {
		sintomas = new ArrayList<>();
		condiciones = new ArrayList<>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Boolean getTrabajaSalud() {
		return trabajaSalud;
	}

	public void setTrabajaSalud(Boolean trabajaSalud) {
		this.trabajaSalud = trabajaSalud;
	}

	public Boolean getContactoContagiado() {
		return contactoContagiado;
	}

	public void setContactoContagiado(Boolean contactoContagiado) {
		this.contactoContagiado = contactoContagiado;
	}

	public List<Sintoma> getSintomas() {
		return sintomas;
	}

	public void setSintomas(List<Sintoma> sintomas) {
		this.sintomas = sintomas;
	}

	public List<Condicion> getCondiciones() {
		return condiciones;
	}

	public void setCondiciones(List<Condicion> condiciones) {
		this.condiciones = condiciones;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	// Función para determinar el resultado de un testVirtual
	public String calcularResultadoTestVirtual() {
		System.out.println("Entra");
		int n = sintomas.size();
		System.out.println("n = " + n);

		boolean riesgoPorSintomas = false;
		for (Sintoma sintoma : sintomas) {
			if (sintoma.getNombre().equals("Fiebre") || (sintoma.getNombre().equals("Dificultad para respirar"))) {
				riesgoPorSintomas = true;
				break;
			}
		}

		if (riesgoPorSintomas) {
			if (n >= 2) {
				return "Riesgo Alto";
			} else {
				return "Riesgo Medio";
			}
		} else {
			if (contactoContagiado) {

				if (trabajaSalud) {
					return "Riesgo Alto";
				} else {
					return "Riesgo Medio";
				}
			} else {

				if (trabajaSalud) {
					return "Riesgo Bajo";
				} else {
					return "Sin Riesgo";
				}
			}
		}
	}

}
