package com.covid19mvc.app.model.entities.publico;

//import java.util.ArrayList;
import java.util.Date;


import com.fasterxml.jackson.annotation.JsonProperty;
//import java.util.List;



public class Datos_API_pais_tiempo {
	
	
	dataDia[] dataDiaArray;
    
    public Datos_API_pais_tiempo() {

    }
    
    public dataDia[] getDataDiaArray() {
    	return dataDiaArray;
    }
    public void setDataDiaArray(dataDia[] dataDiaArray) {
    	this.dataDiaArray=dataDiaArray;
    }
    
    public static class dataDia{
	
		@JsonProperty("Country")
		private String country; //ALA Aland Islands
		
		@JsonProperty("CountryCode")
	    private String countryCode; //AX
		
		@JsonProperty("Lat")
	    private String lat; //0
		
		@JsonProperty("Lon")
	    private String lon; //0
		
		@JsonProperty("Confirmed")
	    private Long confirmed; //0
		
		@JsonProperty("Deaths")
	    private Long deaths; //0
			
		@JsonProperty("Recovered")
	    private Long recovered; //0
		
		@JsonProperty("Active")
	    private Long active; //0
		
		@JsonProperty("Date")
	    private Date date; //2020-04-05T06:37:00Z
		
	    public dataDia() {
	    	
	    } 
	    
		public String getCountry() {
			return country;
		}
	
		public void setCountry(String country) {
			this.country = country;
		}
	
		public String getCountryCode() {
			return countryCode;
		}
	
		public void setCountryCode(String countryCode) {
			this.countryCode = countryCode;
		}
		
		public String getLat() {
			return lat;
		}
	
		public void setLat(String lat) {
			this.lat = lat;
		}
		
		public String getlon() {
			return lon;
		}
	
		public Long getConfirmed() {
			return confirmed;
		}
	
		public void setConfirmed(Long Confirmed) {
			this.confirmed = Confirmed;
		}
	
		public Long getDeaths() {
			return deaths;
		}
	
		public void setDeaths(Long Deaths) {
			this.deaths = Deaths;
		}
	
		public Long getRecovered() {
			return recovered;
		}
	
		public void setRecovered(Long Recovered) {
			this.recovered = Recovered;
		}
	
		public Long getActive() {
			return active;
		}
	
		public void setActice(Long Active) {
			this.active = Active;
		}
	
		public Date getDate() {
			return date;
		}
	
		public void setDate(Date date) {
			this.date = date;
		}		
    }
}
