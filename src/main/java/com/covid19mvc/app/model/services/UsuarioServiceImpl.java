package com.covid19mvc.app.model.services;

import java.util.List;
import java.io.File;
import java.io.FileOutputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.covid19mvc.app.model.daos.IUsuarioDao;
import com.covid19mvc.app.model.entities.TestDescarteClinico;
import com.covid19mvc.app.model.entities.TestDescarteVirtual;
import com.covid19mvc.app.model.entities.Usuario;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


@Service("usuarioservice")
public class UsuarioServiceImpl implements UsuarioService{
	
	@Autowired
	private IUsuarioDao usuarioDao;

	@Override
	@Transactional(readOnly = true)
	public List<Usuario> findAll() {
		return (List<Usuario>) usuarioDao.findAll();
	}

	@Override
	@Transactional
	public void save(Usuario usuario) {
		usuarioDao.save(usuario);
	}

	@Override
	@Transactional(readOnly = true)
	public Usuario findById(Long id) {
		return usuarioDao.findById(id).orElse(null);
	}

	@Override
	public void delete(Long id) {
		usuarioDao.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Usuario findByMail(String mail) {
		return usuarioDao.findByCorreo(mail);
	}

	@Override
	public boolean createPdf(Usuario usuario, ServletContext context, HttpServletRequest request,HttpServletResponse response) {
		Document document = new Document(PageSize.A4,15,15,45,30);
		try {
			String filePath = context.getRealPath("/resources/reports");
			File file = new File(filePath);
			boolean exists = new File(filePath).exists();
			if(!exists) {
				new File(filePath).mkdirs();
			}
			
			PdfWriter writer = PdfWriter.getInstance(document,new FileOutputStream(file+"/"+"usuarios"+".pdf"));
			document.open();
			
			Font mainFont = FontFactory.getFont("Arial",10, BaseColor.BLACK);
			
			Paragraph paragraph = new Paragraph("Reporte generado del usuario", mainFont);
			paragraph.setAlignment(Element.ALIGN_CENTER);
			paragraph.setIndentationLeft(50);
			paragraph.setIndentationRight(50);
			paragraph.setSpacingAfter(10);
			document.add(paragraph);
			
			PdfPTable table = new PdfPTable(4);
			table.setWidthPercentage(100);
			table.setSpacingBefore(10f);
			table.setSpacingAfter(10);
			
			Font tableHeader = FontFactory.getFont("Arial",10,BaseColor.BLACK);
			Font tableBody = FontFactory.getFont("Arial",9,BaseColor.BLACK);
			
			float[] columnWidths = {2f,2f,2f,2f};
			table.setWidths(columnWidths);
			
			PdfPCell nombre = new PdfPCell(new Paragraph("Nombre",tableHeader));
			nombre.setBorderColor(BaseColor.BLACK);
			nombre.setPaddingLeft(10);
			nombre.setHorizontalAlignment(Element.ALIGN_CENTER);
			nombre.setVerticalAlignment(Element.ALIGN_CENTER);
			nombre.setBackgroundColor(BaseColor.GRAY);
			nombre.setExtraParagraphSpace(5f);
			table.addCell(nombre);
			
			PdfPCell apellido = new PdfPCell(new Paragraph("Apellido",tableHeader));
			apellido.setBorderColor(BaseColor.BLACK);
			apellido.setPaddingLeft(10);
			apellido.setHorizontalAlignment(Element.ALIGN_CENTER);
			apellido.setVerticalAlignment(Element.ALIGN_CENTER);
			apellido.setBackgroundColor(BaseColor.GRAY);
			apellido.setExtraParagraphSpace(5f);
			table.addCell(apellido);
			
			PdfPCell pais = new PdfPCell(new Paragraph("Pais",tableHeader));
			pais.setBorderColor(BaseColor.BLACK);
			pais.setPaddingLeft(10);
			pais.setHorizontalAlignment(Element.ALIGN_CENTER);
			pais.setVerticalAlignment(Element.ALIGN_CENTER);
			pais.setBackgroundColor(BaseColor.GRAY);
			pais.setExtraParagraphSpace(5f);
			table.addCell(pais);
			
			PdfPCell sexo = new PdfPCell(new Paragraph("Sexo",tableHeader));
			sexo.setBorderColor(BaseColor.BLACK);
			sexo.setPaddingLeft(10);
			sexo.setHorizontalAlignment(Element.ALIGN_CENTER);
			sexo.setVerticalAlignment(Element.ALIGN_CENTER);
			sexo.setBackgroundColor(BaseColor.GRAY);
			sexo.setExtraParagraphSpace(5f);
			table.addCell(sexo);
			
			if (usuario!= null) {
				
				PdfPCell nombreValue = new PdfPCell(new Paragraph(usuario.getNombre(),tableBody));
				nombreValue.setBorderColor(BaseColor.BLACK);
				nombreValue.setPaddingLeft(10);
				nombreValue.setHorizontalAlignment(Element.ALIGN_CENTER);
				nombreValue.setVerticalAlignment(Element.ALIGN_CENTER);
				nombreValue.setBackgroundColor(BaseColor.WHITE);
				nombreValue.setExtraParagraphSpace(5f);
				table.addCell(nombreValue);
				
				PdfPCell apellidoValue = new PdfPCell(new Paragraph(usuario.getApellido(),tableBody));
				apellidoValue.setBorderColor(BaseColor.BLACK);
				apellidoValue.setPaddingLeft(10);
				apellidoValue.setHorizontalAlignment(Element.ALIGN_CENTER);
				apellidoValue.setVerticalAlignment(Element.ALIGN_CENTER);
				apellidoValue.setBackgroundColor(BaseColor.WHITE);
				apellidoValue.setExtraParagraphSpace(5f);
				table.addCell(apellidoValue);
				
				PdfPCell paisValue = new PdfPCell(new Paragraph(usuario.getPais(),tableBody));
				paisValue.setBorderColor(BaseColor.BLACK);
				paisValue.setPaddingLeft(10);
				paisValue.setHorizontalAlignment(Element.ALIGN_CENTER);
				paisValue.setVerticalAlignment(Element.ALIGN_CENTER);
				paisValue.setBackgroundColor(BaseColor.WHITE);
				paisValue.setExtraParagraphSpace(5f);
				table.addCell(paisValue);
				
				PdfPCell sexoValue = new PdfPCell(new Paragraph(usuario.getSexo(),tableBody));
				sexoValue.setBorderColor(BaseColor.BLACK);
				sexoValue.setPaddingLeft(10);
				sexoValue.setHorizontalAlignment(Element.ALIGN_CENTER);
				sexoValue.setVerticalAlignment(Element.ALIGN_CENTER);
				sexoValue.setBackgroundColor(BaseColor.WHITE);
				sexoValue.setExtraParagraphSpace(5f);
				table.addCell(sexoValue);
				
			}
			document.add(table);
			
			Paragraph paragraph1 = new Paragraph("Test clinicos registrados", mainFont);
			paragraph1.setAlignment(Element.ALIGN_CENTER);
			paragraph1.setIndentationLeft(50);
			paragraph1.setIndentationRight(50);
			paragraph1.setSpacingAfter(10);
			document.add(paragraph1);
			
			PdfPTable table1 = new PdfPTable(3);
			table1.setWidthPercentage(100);
			table1.setSpacingBefore(10f);
			table1.setSpacingAfter(10);
			
			PdfPCell fecha = new PdfPCell(new Paragraph("Fecha",tableHeader));
			fecha.setBorderColor(BaseColor.BLACK);
			fecha.setPaddingLeft(10);
			fecha.setHorizontalAlignment(Element.ALIGN_CENTER);
			fecha.setVerticalAlignment(Element.ALIGN_CENTER);
			fecha.setBackgroundColor(BaseColor.GRAY);
			fecha.setExtraParagraphSpace(5f);
			table1.addCell(fecha);
			
			PdfPCell tipo = new PdfPCell(new Paragraph("Tipo",tableHeader));
			tipo.setBorderColor(BaseColor.BLACK);
			tipo.setPaddingLeft(10);
			tipo.setHorizontalAlignment(Element.ALIGN_CENTER);
			tipo.setVerticalAlignment(Element.ALIGN_CENTER);
			tipo.setBackgroundColor(BaseColor.GRAY);
			tipo.setExtraParagraphSpace(5f);
			table1.addCell(tipo);
			
			PdfPCell resultado = new PdfPCell(new Paragraph("Resultado",tableHeader));
			resultado.setBorderColor(BaseColor.BLACK);
			resultado.setPaddingLeft(10);
			resultado.setHorizontalAlignment(Element.ALIGN_CENTER);
			resultado.setVerticalAlignment(Element.ALIGN_CENTER);
			resultado.setBackgroundColor(BaseColor.GRAY);
			resultado.setExtraParagraphSpace(5f);
			table1.addCell(resultado);
						
			List<TestDescarteClinico> testsClinicos = usuario.getTestsClinicos();
			
			for (TestDescarteClinico testclinico : testsClinicos) {
				
				PdfPCell fechaValue = new PdfPCell(new Paragraph(testclinico.getFecha().toString(),tableBody));
				fechaValue.setBorderColor(BaseColor.BLACK);
				fechaValue.setPaddingLeft(10);
				fechaValue.setHorizontalAlignment(Element.ALIGN_CENTER);
				fechaValue.setVerticalAlignment(Element.ALIGN_CENTER);
				fechaValue.setBackgroundColor(BaseColor.WHITE);
				fechaValue.setExtraParagraphSpace(5f);
				table1.addCell(fechaValue);
				
				PdfPCell tipoValue = new PdfPCell(new Paragraph(testclinico.getTipo(),tableBody));
				tipoValue.setBorderColor(BaseColor.BLACK);
				tipoValue.setPaddingLeft(10);
				tipoValue.setHorizontalAlignment(Element.ALIGN_CENTER);
				tipoValue.setVerticalAlignment(Element.ALIGN_CENTER);
				tipoValue.setBackgroundColor(BaseColor.WHITE);
				tipoValue.setExtraParagraphSpace(5f);
				table1.addCell(tipoValue);
				
				PdfPCell resultadoValue = new PdfPCell(new Paragraph(testclinico.getResultado(),tableBody));
				resultadoValue.setBorderColor(BaseColor.BLACK);
				resultadoValue.setPaddingLeft(10);
				resultadoValue.setHorizontalAlignment(Element.ALIGN_CENTER);
				resultadoValue.setVerticalAlignment(Element.ALIGN_CENTER);
				resultadoValue.setBackgroundColor(BaseColor.WHITE);
				resultadoValue.setExtraParagraphSpace(5f);
				table1.addCell(resultadoValue);
		}		
			
			document.add(table1);
			
			Paragraph paragraph2 = new Paragraph("Test virtual registrado", mainFont);
			paragraph2.setAlignment(Element.ALIGN_CENTER);
			paragraph2.setIndentationLeft(50);
			paragraph2.setIndentationRight(50);
			paragraph2.setSpacingAfter(10);
			document.add(paragraph2);
			
			PdfPTable table2 = new PdfPTable(2);
			table1.setWidthPercentage(100);
			table1.setSpacingBefore(10f);
			table1.setSpacingAfter(10);
			
			PdfPCell fecha1 = new PdfPCell(new Paragraph("Fecha",tableHeader));
			fecha1.setBorderColor(BaseColor.BLACK);
			fecha1.setPaddingLeft(10);
			fecha1.setHorizontalAlignment(Element.ALIGN_CENTER);
			fecha1.setVerticalAlignment(Element.ALIGN_CENTER);
			fecha1.setBackgroundColor(BaseColor.GRAY);
			fecha1.setExtraParagraphSpace(5f);
			table2.addCell(fecha1);
						
			PdfPCell resultado1 = new PdfPCell(new Paragraph("Resultado",tableHeader));
			resultado1.setBorderColor(BaseColor.BLACK);
			resultado1.setPaddingLeft(10);
			resultado1.setHorizontalAlignment(Element.ALIGN_CENTER);
			resultado1.setVerticalAlignment(Element.ALIGN_CENTER);
			resultado1.setBackgroundColor(BaseColor.GRAY);
			resultado1.setExtraParagraphSpace(5f);
			table2.addCell(resultado1);
						
			List<TestDescarteVirtual> testsVirtuales = usuario.getTestsVirtuales();
									
			for (TestDescarteVirtual testvirtual : testsVirtuales) {
				
				PdfPCell fechaValue1 = new PdfPCell(new Paragraph(testvirtual.getFecha().toString(),tableBody));
				fechaValue1.setBorderColor(BaseColor.BLACK);
				fechaValue1.setPaddingLeft(10);
				fechaValue1.setHorizontalAlignment(Element.ALIGN_CENTER);
				fechaValue1.setVerticalAlignment(Element.ALIGN_CENTER);
				fechaValue1.setBackgroundColor(BaseColor.WHITE);
				fechaValue1.setExtraParagraphSpace(5f);
				table2.addCell(fechaValue1);
								
				PdfPCell resultadoValue = new PdfPCell(new Paragraph(testvirtual.getResultado(),tableBody));
				resultadoValue.setBorderColor(BaseColor.BLACK);
				resultadoValue.setPaddingLeft(10);
				resultadoValue.setHorizontalAlignment(Element.ALIGN_CENTER);
				resultadoValue.setVerticalAlignment(Element.ALIGN_CENTER);
				resultadoValue.setBackgroundColor(BaseColor.WHITE);
				resultadoValue.setExtraParagraphSpace(5f);
				table2.addCell(resultadoValue);
					
		}		
			
			document.add(table2);
			document.close();
			writer.close();
			
			return true;

		}catch (Exception e) {
			return false;
		}

	}

}
