package com.covid19mvc.app.model.services;

import com.covid19mvc.app.model.entities.Role;

public interface RoleService {
	
	public Role findByAuthority(String authority);
}
