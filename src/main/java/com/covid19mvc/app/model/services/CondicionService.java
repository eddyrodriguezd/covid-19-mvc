package com.covid19mvc.app.model.services;

import java.util.List;

import com.covid19mvc.app.model.entities.Condicion;

public interface CondicionService {
	
	public List<Condicion> findAll();

	public void save(Condicion condicion);
	
	public Condicion findById(Integer id);
	
	public void delete(Integer id);
}
