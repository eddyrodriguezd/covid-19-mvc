package com.covid19mvc.app.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.covid19mvc.app.model.daos.ITestDescarteClinicoDao;
import com.covid19mvc.app.model.entities.TestDescarteClinico;

@Service
public class TestDescarteClinicoServiceImpl implements TestDescarteClinicoService {
	
	@Autowired
	private ITestDescarteClinicoDao testDescarteClinicoDao;

	@Override
	@Transactional(readOnly = true)
	public List<TestDescarteClinico> findAll() {
		return (List<TestDescarteClinico>) testDescarteClinicoDao.findAll();
	}

	@Override
	@Transactional
	public void save(TestDescarteClinico testDescarteClinico) {
		testDescarteClinicoDao.save(testDescarteClinico);
	}

	@Override
	@Transactional(readOnly = true)
	public TestDescarteClinico findById(Long id) {
		return testDescarteClinicoDao.findById(id).orElse(null);
	}

	@Override
	public void delete(Long id) {
		testDescarteClinicoDao.deleteById(id);
	}

}
