package com.covid19mvc.app.model.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.covid19mvc.app.model.entities.Role;
import com.covid19mvc.app.model.entities.User;

@Service
public class JPAUserDetailsService implements UserDetailsService{

	@Autowired
	private UserService userService;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@Override
	@Transactional(readOnly=true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userService.findByMail(username);
		
		if(user == null) {
			throw new UsernameNotFoundException("Error login");
		}
		
		List<GrantedAuthority> authorities = new ArrayList<>();
		for (Role role: user.getRoles()) {
			authorities.add(new SimpleGrantedAuthority(role.getAuthority()));
		}
		
		return new org.springframework.security.core.userdetails.User(user.getMail(), user.getPassword(), user.getEnabled(), 
				true, true, true, authorities);
	}
	
	public boolean saveUser(User user) {
		
		//Verificar si existe usuario con ese correo
		if(userService.findByMail(user.getMail())==null) {
			//Encriptación de contraseña
			user.setPassword(passwordEncoder.encode(user.getPassword()));
			//Habilitación
			user.setEnabled(true);
			//Asignación de rol "user" al usuario recién creado
			List<Role> rolesUsuario = new ArrayList<>();
			Role role = new Role();
			role.setAuthority("ROLE_USER");
			rolesUsuario.add(role);
			user.setRoles(rolesUsuario);
			//Guardar en la BD
			userService.save(user);
			//Setear autenticación para que ingrese directamente
			List<GrantedAuthority> authorities = new ArrayList<>();
			authorities.add(new SimpleGrantedAuthority(user.getRoles().get(0).getAuthority()));
			Authentication auth = new UsernamePasswordAuthenticationToken(user.getMail(), user.getPassword(), authorities);
			SecurityContextHolder.getContext().setAuthentication(auth);
			return true;
		}
		else {
			return false;
		}
		
	}

}
