package com.covid19mvc.app.model.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.covid19mvc.app.model.daos.IUserDao;
import com.covid19mvc.app.model.entities.User;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private IUserDao userDao;
	
	@Override
	@Transactional
	public void save(User user) {
		userDao.save(user);
	}

	@Override
	@Transactional(readOnly = true)
	public User findByMail(String mail) {
		return userDao.findByMail(mail);		
	}

}
