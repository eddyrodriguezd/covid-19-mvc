package com.covid19mvc.app.model.services;

import java.util.List;

import com.covid19mvc.app.model.entities.Sintoma;

public interface SintomaService {
	
	public List<Sintoma> findAll();

	public void save(Sintoma sintoma);
	
	public Sintoma findById(Integer id);
	
	public void delete(Integer id);
}