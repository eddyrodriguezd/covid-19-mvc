package com.covid19mvc.app.model.services;

import com.covid19mvc.app.model.daos.IRoleDao;
import com.covid19mvc.app.model.entities.Role;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RoleServiceImpl implements RoleService{

	@Autowired
	IRoleDao roleDao;
	
	@Override
	@Transactional(readOnly = true)
	public Role findByAuthority(String authority) {
		return roleDao.findByAuthority(authority);
	}

}
