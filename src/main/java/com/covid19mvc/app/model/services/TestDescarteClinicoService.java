package com.covid19mvc.app.model.services;

import java.util.List;

import com.covid19mvc.app.model.entities.TestDescarteClinico;

public interface TestDescarteClinicoService {
	
	public List<TestDescarteClinico> findAll();

	public void save(TestDescarteClinico testDescarteClinico);
	
	public TestDescarteClinico findById(Long id);
	
	public void delete(Long id);
}
