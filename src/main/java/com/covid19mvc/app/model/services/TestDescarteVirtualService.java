package com.covid19mvc.app.model.services;

import java.util.List;

import com.covid19mvc.app.model.entities.TestDescarteVirtual;;

public interface TestDescarteVirtualService {
	
	public List<TestDescarteVirtual> findAll();

	public void save(TestDescarteVirtual testDescarteVirtual);
	
	public TestDescarteVirtual findById(Long id);
	
	public void delete(Long id);
}
