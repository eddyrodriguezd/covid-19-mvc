package com.covid19mvc.app.model.services;

import com.covid19mvc.app.model.entities.User;

public interface UserService {

	public void save(User user);
	
	public User findByMail(String mail);
}
