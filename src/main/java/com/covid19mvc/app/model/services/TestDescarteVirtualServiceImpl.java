package com.covid19mvc.app.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.covid19mvc.app.model.daos.ITestDescarteVirtualDao;
import com.covid19mvc.app.model.entities.TestDescarteVirtual;

@Service
public class TestDescarteVirtualServiceImpl implements TestDescarteVirtualService {

	
	@Autowired
	private ITestDescarteVirtualDao testDescarteVirtualDao;

	@Override
	@Transactional(readOnly = true)
	public List<TestDescarteVirtual> findAll() {
		return (List<TestDescarteVirtual>) testDescarteVirtualDao.findAll();
	}

	@Override
	@Transactional
	public void save(TestDescarteVirtual testDescarteVirtual) {
		testDescarteVirtualDao.save(testDescarteVirtual);
	}

	@Override
	@Transactional(readOnly = true)
	public TestDescarteVirtual findById(Long id) {
		return testDescarteVirtualDao.findById(id).orElse(null);
	}

	@Override
	public void delete(Long id) {
		testDescarteVirtualDao.deleteById(id);
	}

}
