package com.covid19mvc.app.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.covid19mvc.app.model.daos.ICondicionDao;
import com.covid19mvc.app.model.entities.Condicion;

@Service
public class CondicionServiceImpl implements CondicionService {
	
	@Autowired
	private ICondicionDao condicionDao;

	@Override
	@Transactional(readOnly = true)
	public List<Condicion> findAll() {
		return (List<Condicion>) condicionDao.findAll();
	}

	@Override
	@Transactional
	public void save(Condicion condicion) {
		condicionDao.save(condicion);
	}

	@Override
	@Transactional(readOnly = true)
	public Condicion findById(Integer id) {
		return condicionDao.findById(id).orElse(null);
	}

	@Override
	public void delete(Integer id) {
		condicionDao.deleteById(id);
	}

}
