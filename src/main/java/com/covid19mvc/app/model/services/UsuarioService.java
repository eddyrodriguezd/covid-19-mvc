package com.covid19mvc.app.model.services;

import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.covid19mvc.app.model.entities.Usuario;;

public interface UsuarioService {
	
	public List<Usuario> findAll();

	public void save(Usuario usuario);
	
	public Usuario findById(Long id);
	
	public void delete(Long id);
	
	public Usuario findByMail(String mail);

	public boolean createPdf(Usuario usuario, ServletContext context, HttpServletRequest request,
			HttpServletResponse response);
}

