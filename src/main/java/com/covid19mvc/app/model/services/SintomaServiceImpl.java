package com.covid19mvc.app.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.covid19mvc.app.model.daos.ISintomaDao;
import com.covid19mvc.app.model.entities.Sintoma;

@Service
public class SintomaServiceImpl implements SintomaService {
	
	@Autowired
	private ISintomaDao sintomaDao;

	@Override
	@Transactional(readOnly = true)
	public List<Sintoma> findAll() {
		return (List<Sintoma>) sintomaDao.findAll();
	}

	@Override
	@Transactional
	public void save(Sintoma sintoma) {
		sintomaDao.save(sintoma);
	}

	@Override
	@Transactional(readOnly = true)
	public Sintoma findById(Integer id) {
		return sintomaDao.findById(id).orElse(null);
	}

	@Override
	public void delete(Integer id) {
		sintomaDao.deleteById(id);
	}

}
