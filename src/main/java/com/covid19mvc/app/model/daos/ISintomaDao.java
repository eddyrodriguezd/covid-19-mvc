package com.covid19mvc.app.model.daos;

import org.springframework.data.repository.CrudRepository;

import com.covid19mvc.app.model.entities.Sintoma;

public interface ISintomaDao extends CrudRepository<Sintoma, Integer>{

}
