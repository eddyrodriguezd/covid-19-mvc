package com.covid19mvc.app.model.daos;

import org.springframework.data.repository.CrudRepository;
import com.covid19mvc.app.model.entities.Role;

public interface IRoleDao extends CrudRepository<Role, Long>{

	public Role findByAuthority(String authority);
}
