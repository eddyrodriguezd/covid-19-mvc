package com.covid19mvc.app.model.daos;

import org.springframework.data.repository.CrudRepository;

import com.covid19mvc.app.model.entities.User;

public interface IUserDao extends CrudRepository<User, Long>{
	
	public User findByMail(String mail);
}
