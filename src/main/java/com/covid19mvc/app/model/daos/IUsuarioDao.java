package com.covid19mvc.app.model.daos;

import org.springframework.data.repository.CrudRepository;

import com.covid19mvc.app.model.entities.Usuario;

public interface IUsuarioDao extends CrudRepository<Usuario, Long>{

	public Usuario findByCorreo(String mail);
}

