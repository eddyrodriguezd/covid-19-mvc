package com.covid19mvc.app.model.daos;

import org.springframework.data.repository.CrudRepository;

import com.covid19mvc.app.model.entities.Condicion;

public interface ICondicionDao extends CrudRepository<Condicion, Integer>{

}
