package com.covid19mvc.app.model.daos;

import org.springframework.data.repository.CrudRepository;

import com.covid19mvc.app.model.entities.TestDescarteClinico;

public interface ITestDescarteClinicoDao extends CrudRepository<TestDescarteClinico, Long>{

}

