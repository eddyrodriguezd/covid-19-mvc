package com.covid19mvc.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Covid19MvcApplication {
	
	public static void main(String[] args) {
		
		SpringApplication.run(Covid19MvcApplication.class, args);
	    /*final String uri = "https://api.covid19api.com/summary";
	     
	    RestTemplate restTemplate = new RestTemplate();
	    DatosAPI result = restTemplate.getForObject(uri, DatosAPI.class);	    
	    Arrays.sort(result.getCountries(), Comparator.comparing(PaisAPI::getTotalConfirmed).reversed());	
	    System.out.println(result.getCountries()[0].getCountry());*/
		
	}

}
