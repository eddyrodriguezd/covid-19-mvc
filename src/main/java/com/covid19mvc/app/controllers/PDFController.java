package com.covid19mvc.app.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;


import com.covid19mvc.app.model.services.UsuarioService;
import com.covid19mvc.app.model.entities.Usuario;


@Controller 
public class PDFController {

	@Autowired private UsuarioService usuarioService;
	@Autowired private ServletContext context;
	
	@GetMapping(value="/menu/reportes")
	public String allUsuarios(Model model) {
		
		String currentUserName="";
		Usuario usuario = null;
								
		 Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		    if (!(authentication instanceof AnonymousAuthenticationToken)) {
		        currentUserName = authentication.getName();
		        if(currentUserName!="") {
		        	usuario = usuarioService.findByMail(currentUserName);
		        }
		    }
		
		    if (usuario!= null) {
			    
		    	model.addAttribute("usuarios",usuario);
		    	model.addAttribute("tests_clinicos",usuario.getTestsClinicos());
		    	model.addAttribute("tests_virtuales",usuario.getTestsVirtuales());
		    	 	
		    	return "reportes";
			}
		    return "redirect:/menu";  
	}
	
	@GetMapping(value="/menu/reportes/createPdf")
	public void createPdf(HttpServletRequest request, HttpServletResponse response) {
		
		String currentUserName="";
		Usuario usuario = null;
		
								
		 Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		    if (!(authentication instanceof AnonymousAuthenticationToken)) {
		        currentUserName = authentication.getName();
		        if(currentUserName!="") {
		        	usuario = usuarioService.findByMail(currentUserName);
		        	
		        	 }
		    }
		    if (usuario!= null) {
		    	    	
		    	boolean isFlag = usuarioService.createPdf(usuario,context,request,response);
		    	
		    		if(isFlag) {
		    			String fullPath = request.getServletContext().getRealPath("/resources/reports/"+"usuarios"+".pdf");
		    			filedownload(fullPath,response,"reporte.pdf");
		    	}
		    }
	}

	private void filedownload(String fullPath, HttpServletResponse response, String fileName) {
		
		File file = new File(fullPath);
		final int BUFFER_SIZE = 4096;
		if(file.exists()) {
			try {
				FileInputStream inputStream = new FileInputStream(file);
				String mimeType = context.getMimeType(fullPath);
				response.setContentType(mimeType);
				response.setHeader("content-disposition", "attachment;filename="+fileName);
				OutputStream outputStream = response.getOutputStream();
				byte[] buffer = new byte[BUFFER_SIZE];
				int bytesRead = -1;
				while((bytesRead = inputStream.read(buffer))!=-1){
					outputStream.write(buffer,0,bytesRead);
				}
				inputStream.close();
				outputStream.close();
				file.delete();
				
			}catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
	}
	
}
