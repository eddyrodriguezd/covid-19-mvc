package com.covid19mvc.app.controllers;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.covid19mvc.app.model.entities.TestDescarteClinico;
import com.covid19mvc.app.model.entities.Usuario;
import com.covid19mvc.app.model.services.UsuarioService;


@Controller
@RequestMapping("/testclinico")
public class TestClinicoController {
	
	@Autowired
	private UsuarioService usuarioService;
	
	@PostMapping("/registrar")
	public String registrarTC(@Valid /*@RequestBody*/ TestDescarteClinico testClinico, RedirectAttributes flash) {
		String currentUserName="";
		Usuario usuario = null;
		
	    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	    if (!(authentication instanceof AnonymousAuthenticationToken)) {
	        currentUserName = authentication.getName();
	        if(currentUserName!="") {
	        	usuario = usuarioService.findByMail(currentUserName);
		    }
	    }
	    
	    if(usuario!= null) {
		    usuario.addTestClinico(testClinico);
			usuarioService.save(usuario);
			
			flash.addFlashAttribute("test", "Test clínico agregado con éxito");
	    }
	    else {
			flash.addFlashAttribute("error", "Error en el proceso");
	    }
	    return "redirect:/menu";

	}
	
	/*@GetMapping("/listar")
	public List<TestDescarteClinico> listar() {
		return testService.findAll();
	}
	
	@DeleteMapping("/eliminar")
	public void delete(@RequestParam Long id) {
		testService.delete(id);
	}*/
}
