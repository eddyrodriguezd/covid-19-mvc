package com.covid19mvc.app.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.covid19mvc.app.model.entities.Condicion;
import com.covid19mvc.app.model.entities.Sintoma;
import com.covid19mvc.app.model.entities.TestDescarteVirtual;
import com.covid19mvc.app.model.entities.Usuario;
import com.covid19mvc.app.model.services.UsuarioService;

@SuppressWarnings("unused")
@Controller
@RequestMapping("/testvirtual")

public class TestVirtualController {
	
	@Autowired
	private UsuarioService usuarioService;
	
	
	@PostMapping("/registrar")
	public String registrarTC(@Valid /*@RequestBody*/ TestDescarteVirtual testVirtual, RedirectAttributes flash) {
		
		
		String currentUserName="";
		Usuario usuario = null;
		
	    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	    if (!(authentication instanceof AnonymousAuthenticationToken)) {
	        currentUserName = authentication.getName();
	        System.out.println("1");
	        System.out.println(currentUserName);
	        if(currentUserName!="") {
	        	usuario = usuarioService.findByMail(currentUserName);
	        	System.out.println("2");
		        System.out.println(currentUserName);
		    }
	    }
		
	    	    
	    if(usuario!= null) {
	    	//Asignación de resultado en función a síntomas seleccionados
			testVirtual.setResultado(testVirtual.calcularResultadoTestVirtual());
			    
		    usuario.addTestVirtual(testVirtual);
		    usuarioService.save(usuario);

			flash.addFlashAttribute("test", "Test virtual agregado con éxito");
			return "redirect:/menu";
	    }
	    else {
			flash.addFlashAttribute("error", "Error en el proceso");
			return "redirect:/menu";
	    }

	}
	
}
