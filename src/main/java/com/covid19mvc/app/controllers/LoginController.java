package com.covid19mvc.app.controllers;

import java.security.Principal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.covid19mvc.app.model.entities.Usuario;
import com.covid19mvc.app.model.services.JPAUserDetailsService;
import com.covid19mvc.app.model.services.UsuarioService;

@Controller
public class LoginController {
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private JPAUserDetailsService userService;
	
	@GetMapping("/login")
	public String login(Model model, Principal principal, RedirectAttributes flash, 
			@RequestParam (value="error", required=false) String error,
			@RequestParam (value="logout", required=false) String logout) {
		
		if(principal != null) {
			flash.addFlashAttribute("inicio", "Ya ha iniciado sesión");
			return "redirect:/menu";
		}
		
		if(error!=null) {
			model.addAttribute("error", "Credenciales incorrectas");
		}
		
		if(logout!=null) {
			flash.addFlashAttribute("cierre", "Ha cerrado sesión con éxito");
			return "redirect:/";
		}
		
		return "login";
	}
	
	@GetMapping("/signup")
	public String signUp(Model model, Principal principal, RedirectAttributes flash) {
		
		if(principal != null) {
			flash.addFlashAttribute("inicio", "Ya ha iniciado sesión");
			return "redirect:/menu";
		}
		
		return "signup";
	}
	
	@PostMapping("/signup")
	public String registro(@Valid /*@RequestBody*/ Usuario usuario, String password, RedirectAttributes flash) {
		if(usuario!= null) {			
			com.covid19mvc.app.model.entities.User user = new com.covid19mvc.app.model.entities.User();
			user.setMail(usuario.getCorreo());
			user.setPassword(password);
			if(userService.saveUser(user)) {
				usuarioService.save(usuario);
				flash.addFlashAttribute("test", "Nuevo usuario creado con éxito");
				return "redirect:/menu";
			}
			else {
				flash.addFlashAttribute("error", "Ya existe un usuario con ese correo");
				return "redirect:/signup";
			}
			

	    }
	    else {
			flash.addFlashAttribute("error", "Error en el proceso");
			return "redirect:/signup";
	    }
		
	}


}
