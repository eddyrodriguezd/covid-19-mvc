package com.covid19mvc.app.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
//import org.json;

import com.covid19mvc.app.model.entities.publico.DatosAPI;
import com.covid19mvc.app.model.entities.publico.DatosAPI.PaisAPI;

import com.covid19mvc.app.model.services.UsuarioService;
import com.covid19mvc.app.model.entities.Usuario;

import com.covid19mvc.app.model.services.TestDescarteClinicoService;
import com.covid19mvc.app.model.entities.TestDescarteClinico;

import com.covid19mvc.app.model.services.TestDescarteVirtualService;
import com.covid19mvc.app.model.entities.TestDescarteVirtual;


@RestController
public class servicioController {

    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private TestDescarteClinicoService testClinicoService;
    @Autowired
    private TestDescarteVirtualService testVirtualService;

    @GetMapping("/servicio")
    public StringBuilder getUsuarios() {
    	List<Usuario> listaUsuarios=usuarioService.findAll();
    	
    	final StringBuilder objJson = new StringBuilder();
    	StringBuilder jsonUsuarios;
    	
    	objJson.append("{");
    	objJson.append((char)34+"resultadosUsuario"+(char)34+":");
    	objJson.append("[");
    	for(int i=0;i<listaUsuarios.size();i++) {   		
    		
    		Usuario var=listaUsuarios.get(i);
    		
    		
    		objJson.append("{");
    		//objJson.append((char)34+"posicion"+(char)34+":"+(char)34+i+(char)34);
    		
    		//objJson.append(",");
    		
    		List<TestDescarteClinico> descartesClinicos = var.getTestsClinicos();
    		//objJson.append((char)34+"testsDescarteClinico{"+(char)34);
    		objJson.append((char)34+"testsDescarteClinico"+(char)34+":[");
    		for(int m=0;m<descartesClinicos.size();m++) {   			
    			objJson.append("{");
    			objJson.append((char)34+"tipo"+(char)34+":").append((char)34+descartesClinicos.get(m).getTipo()+(char)34);
        		objJson.append(","+(char)34+"fecha"+(char)34+":").append((char)34+descartesClinicos.get(m).getFecha().toString()+(char)34);
        		objJson.append(","+(char)34+"resultado"+(char)34+":").append((char)34+descartesClinicos.get(m).getResultado()+(char)34);
    			objJson.append("}");
    			if(m!=(descartesClinicos.size()-1))objJson.append(",");
    		}
    		objJson.append("]");
    		
    		objJson.append(",");
    		
    		List<TestDescarteVirtual> descarteVirtual = var.getTestsVirtuales();
    		objJson.append((char)34+"testsDescarteVirtual"+(char)34+":[");
    		for(int m=0;m<descarteVirtual.size();m++) {
    			objJson.append("{");
    			objJson.append((char)34+"fecha"+(char)34+":").append((char)34+descarteVirtual.get(m).getFecha().toString()+(char)34);
        		objJson.append(","+(char)34+"contactoContagiado"+(char)34+":").append((char)34+descarteVirtual.get(m).getContactoContagiado().toString()+(char)34);
        		objJson.append(","+(char)34+"trabajaSalud"+(char)34+":").append((char)34+descarteVirtual.get(m).getTrabajaSalud().toString()+(char)34);
        		objJson.append(","+(char)34+"resultado"+(char)34+":").append((char)34+descarteVirtual.get(m).getResultado()+(char)34);
    			objJson.append("}");
    			if(m!=(descarteVirtual.size()-1))objJson.append(",");
    		}
    		objJson.append("]");

    		objJson.append("}");
    		if(i!=(listaUsuarios.size()-1))objJson.append(",");
    	}
    	objJson.append("]");
    	objJson.append('}');
    	
    	jsonUsuarios=objJson;
        return jsonUsuarios;
    }
}
