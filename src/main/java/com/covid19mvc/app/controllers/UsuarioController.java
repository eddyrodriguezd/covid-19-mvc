package com.covid19mvc.app.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import com.covid19mvc.app.model.entities.publico.DatosAPI;
import com.covid19mvc.app.model.entities.publico.DatosAPI.PaisAPI;

@Controller
public class UsuarioController {
	
	@GetMapping({"/", "/index"})
	public String index(Model model) {		
		final String uri = "https://api.covid19api.com/summary";
		
		RestTemplate restTemplate = new RestTemplate();
		DatosAPI result = restTemplate.getForObject(uri, DatosAPI.class);	    
	    Arrays.sort(result.getCountries(), Comparator.comparing(PaisAPI::getTotalConfirmed).reversed());
	    
	    List<PaisAPI> paisesTop = new ArrayList<>();
	    
	    for(int i=0; i<15; i++) {
	    	paisesTop.add(result.getCountries()[i]);
	    }		
	    
		model.addAttribute("paisesTop", paisesTop);
		
		return "index";
	}
	
	@GetMapping("/menu")
	public String menu() {		
		return "menu";
	}	
	
	@GetMapping("/graficas")
	public String graficas(Model model) {
		return "graficas";
	}
	
	@GetMapping("/prueba")
	public String prueba(Model model) {
		return "NewFile";
	}
	
	@GetMapping("/menu/test-clinico")
	public String testclinico() {
		return "test-clinico";
	}
	
	@RequestMapping(value = "/menu/test-virtual", method=RequestMethod.GET)
	public String testvirtual() {
		return "test-virtual";
	}
	
	
}
